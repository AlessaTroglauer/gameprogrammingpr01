using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

namespace GCTHREEPROne
{

public class MainMenu : MonoBehaviour
{
        //Reference to the menus
        [SerializeField] private GameObject menu;
        [SerializeField] private GameObject menuInGame;
        [SerializeField] private GameObject player;
        [SerializeField] private GameObject credits;

        // void to quit the game
        public void Quit()
        {
        Application.Quit();
        }
        //Void to start the game and hide the UI
        public void StartGame()
        {
            menu.SetActive(false);
            menuInGame.SetActive(true);
            player.SetActive(true);
        }
        //deactivates the in game ui and credits and activates main menu UI
        public void BackToMenu()
        {
            menu.SetActive(true);
            menuInGame.SetActive(false);
            player.SetActive(false);
            credits.SetActive(false);
        }
        //loads the next level
        public void Land()
        {
            SceneManager.LoadScene("Level_02");
        }
        //Opens the credits panel
        public void Credits()
        {
            menu.SetActive(false);
            credits.SetActive(true);
        }
}

}
