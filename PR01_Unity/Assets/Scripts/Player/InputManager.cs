using UnityEngine;
using GCTHREEPROne;
using System.Collections;

public class InputManager : MonoBehaviour
{
#pragma warning disable 649
    // reference to the movement script
    [SerializeField] Movement movement;
    //refrence to the mouselook script
    [SerializeField] MouseLook mouseLook;

    //The playercontrols we created
    PlayerControl controls;
    //The groundmovement actions
    PlayerControl.GroundMovementActions groundMovement;

    //Vector 2 of the horizontal input and mouse input
    Vector2 horizontalInput;
    Vector2 mouseInput;

    private void Awake()
    {
        //sets the controls to the player controls and groundmovement to the groundmovement actions
        controls = new PlayerControl();
        groundMovement = controls.GroundMovement;

        //Reads the value of the movement and mouse look scripts and sets them to the input actions
        groundMovement.HorizontalMovement.performed += ctx => horizontalInput = ctx.ReadValue<Vector2>();

        groundMovement.Jump.performed += _ => movement.OnJumpPressed();
        
        groundMovement.MouseX.performed += ctx => mouseInput.x = ctx.ReadValue<float>();
        groundMovement.MouseY.performed += ctx => mouseInput.y = ctx.ReadValue<float>();

    }

    private void Update()
    {
        //Makes the charcter and mouse move
        movement.ReceiveInput(horizontalInput);
        mouseLook.ReceiveInput(mouseInput);
    }

    //enable
    private void OnEnable()
    {
        controls.Enable();
    }
    //disable
    private void OnDestroy()
    {
        controls.Disable();
    }
}