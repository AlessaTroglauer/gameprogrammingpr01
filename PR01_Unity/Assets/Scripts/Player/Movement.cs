using UnityEngine;
using GCTHREEPROne;

public class Movement : MonoBehaviour
{
#pragma warning disable 649

    //Reference to the charcontroller
    [SerializeField] CharacterController controller;
    //Float for the movement speed
    [SerializeField] float speed = 11f;
    //Vector 2 input to move the player
    Vector2 horizontalInput;

    //Float to set the jump height
    [SerializeField] float jumpHeight = 3.5f;
    //Bool to check if we are jumping
    bool jump;

    //Float to set the gravity
    [SerializeField] float gravity = -30f; // -9.81
    //Vector 3 to set the velocity of the jump
    Vector3 verticalVelocity = Vector3.zero;
    //Layermask to detect what is ground
    [SerializeField] LayerMask groundMask;
    //Bool to check if we are grounded
    bool isGrounded;

    private void Update()
    {
        //Set is grounded to wether or not the sphere is colliding with the ground mask
        isGrounded = Physics.CheckSphere(transform.position, 0.1f, groundMask);
        //if we are grounded we dont have any vertical velocity (not jumping)
        if (isGrounded)
        {
            verticalVelocity.y = 0;
        }
        //Movement of the character forward, backward, left, right
        Vector3 horizontalVelocity = (transform.right * horizontalInput.x + transform.forward * horizontalInput.y) * speed;
        //movement scaled by delta time
        controller.Move(horizontalVelocity * Time.deltaTime);

        // If we are jumping
        if (jump)
        {
            //If we are still grounded
            if (isGrounded)
            {
                //Add upwards velocity
                verticalVelocity.y = Mathf.Sqrt(-2f * jumpHeight * gravity);
            }
            //set bool to false
            jump = false;
        }
        //Jump scaled by time and adding gravity
        verticalVelocity.y += gravity * Time.deltaTime;
        //Movement upwards times velocity and scaled by time
        controller.Move(verticalVelocity * Time.deltaTime);
    }

    //Receicing the vector 2 input to move
    public void ReceiveInput(Vector2 _horizontalInput)
    {
        horizontalInput = _horizontalInput;
    }

    //If jump was pressed set the bool to false
    public void OnJumpPressed()
    {
        jump = true;
    }

}