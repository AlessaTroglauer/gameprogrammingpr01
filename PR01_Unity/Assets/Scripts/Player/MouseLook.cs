using UnityEngine;
using GCTHREEPROne;

public class MouseLook : MonoBehaviour
{
#pragma warning disable 649
    //Sensitivity of the mouse (X axis)
    [SerializeField] float sensitivityX = 8f;
    //Sensitivity of the mouse (Y axis)
    [SerializeField] float sensitivityY = 0.5f;
    //Float for the mouse axes
    float mouseX, mouseY;

    //Transform variables of the camera
    [SerializeField] Transform playerCamera;
    //Clamping the camera on the x axis
    [SerializeField] float xClamp = 85f;
    //Rotation of the camera on x axis
    float xRotation = 0f;

    private void Start()
    {
        //Lock the curos in the middle and hides it
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void Update()
    {
        //Rotates the camera according to the mouse and scaled by time
        transform.Rotate(Vector3.up, mouseX * Time.deltaTime);

        //Actual rotation input of the mouse
        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -xClamp, xClamp);
        Vector3 targetRotation = transform.eulerAngles;
        targetRotation.x = xRotation;
        playerCamera.eulerAngles = targetRotation;
    }
    //recieves the input as Vector2 times the desired sensitivity
    public void ReceiveInput(Vector2 mouseInput)
    {
        mouseX = mouseInput.x * sensitivityX;
        mouseY = mouseInput.y * sensitivityY;
    }

}