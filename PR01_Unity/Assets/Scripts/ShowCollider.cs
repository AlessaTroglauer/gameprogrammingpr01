using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GCTHREEPROne;

public class ShowCollider : MonoBehaviour
{
    //Lets the box collider show up in the scene even when not selected
    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(transform.position, transform.lossyScale);
    }
}