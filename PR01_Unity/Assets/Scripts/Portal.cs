using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using GCTHREEPROne;

public class Portal : MonoBehaviour
{
    //A string that descirbes the sceneName
    [SerializeField] string sceneName;
    //When we enter the collider
    private void OnTriggerEnter(Collider other)
    {
        //Enable mouse again
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        //we load the scene with the corresponding scene name
        SceneManager.LoadScene(sceneName);
    }

}
